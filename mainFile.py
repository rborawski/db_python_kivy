from kivy.config import Config
Config.set('graphics', 'resizable', '0')

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup
from kivy.core.window import Window
from kivy.uix.slider import Slider
from kivy.uix.gridlayout import GridLayout
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.properties import BooleanProperty, ListProperty, StringProperty, ObjectProperty
from kivy.uix.recyclegridlayout import RecycleGridLayout
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.spinner import Spinner
from kivy.base import runTouchApp
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput 

import mysql.connector

Window.size = (1280, 720)

dbconfig = {'host': '127.0.0.1', 
            'user': 'root',
            'password': 'password',
            'database': 'application_items_db', }

conn = mysql.connector.connect(**dbconfig)
cursor = conn.cursor()

Builder.load_file('StatisticsPopup.kv')
Builder.load_file('AnotherScreen.kv')
Builder.load_file('EquipmentPopup.kv')

############################## GLOBAL FUNCTIONS AND VARIABLES ##############################################

def getItemId(item_id: str) -> int:
    _SQL = "SELECT " + item_id + " FROM current_items WHERE id=1"
    cursor.execute(_SQL)
    res = cursor.fetchall()
    return res[0][0]

def getValueFromTable(parameter: str, table_name: str, id: int) -> int:
    _SQL = "SELECT " + parameter + " FROM " + table_name + " WHERE id=" + str(id)
    cursor.execute(_SQL)
    res = cursor.fetchall()
    return res[0][0]

def getStatisticsFromTable(parameter: str) -> int:

    return getValueFromTable(parameter, table_name_head, head_id) +\
         getValueFromTable(parameter, table_name_shoulders, shoulders_id) +\
          getValueFromTable(parameter, table_name_chest, chest_id) +\
           getValueFromTable(parameter, table_name_gloves, gloves_id) +\
            getValueFromTable(parameter, table_name_weapon, weapon_id) +\
             getValueFromTable(parameter, table_name_boots, boots_id)

parameter_armor_name = 'armor_name'
parameter_strength = 'strength'
parameter_dexterity = 'dexterity'
parameter_vitality = 'vitality'
parameter_energy = 'energy'
parameter_defence = 'defence'

table_name_head = 'head_items'
table_name_shoulders = 'shoulder_items'
table_name_chest = 'chest_items'
table_name_gloves = 'gloves_items'
table_name_weapon = 'weapon_items'
table_name_boots = 'boots_items'

head_id = getItemId('head_id')
shoulders_id = getItemId('shoulder_id')
chest_id = getItemId('chest_id')
gloves_id = getItemId('gloves_id')
weapon_id = getItemId('weapon_id')
boots_id = getItemId('boots_id')

############################## CLASSES ##############################################

class Statistics():

    def headStats(self, parameter):
        return getValueFromTable(parameter, table_name_head, head_id)

    def shouldersStats(self, parameter):
        return getValueFromTable(parameter, table_name_shoulders, shoulders_id)

    def chestStats(self, parameter):
        return getValueFromTable(parameter, table_name_chest, chest_id)

    def glovesStats(self, parameter):
        return getValueFromTable(parameter, table_name_gloves, gloves_id)

    def weaponStats(self, parameter):
        return getValueFromTable(parameter, table_name_weapon, weapon_id)

    def bootsStats(self, parameter):
        return getValueFromTable(parameter, table_name_boots, boots_id)


class StatisticsPopup(Popup):
    
    def open(self):
        super().open()


    def statisticsStrength(self):
        return getStatisticsFromTable(parameter_strength)

    def statisticsDexterity(self):
        return getStatisticsFromTable(parameter_dexterity)

    def statisticsVitality(self):
        return getStatisticsFromTable(parameter_vitality)

    def statisticsEnergy(self):
        return getStatisticsFromTable(parameter_energy)

    def statisticsDefence(self):
        return getStatisticsFromTable(parameter_defence)


class MainScreen(Screen):
    statistics = Statistics()

    def dbClose(self):
        cursor.close()
        conn.close()

class EquipItemPopup(Popup):
    obj = ObjectProperty(None)

    def __init__(self, obj, **kwargs):
        super(EquipItemPopup, self).__init__(**kwargs)
        self.obj = obj

class SelectableRecycleGridLayout(FocusBehavior, LayoutSelectionBehavior,
                                  RecycleGridLayout):
    pass

class SelectableButton(RecycleDataViewBehavior, Button):
    ''' Add selection support to the Button '''
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)
    disabled = False

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        return super(SelectableButton, self).refresh_view_attrs(rv, index, data)

    def on_touch_down(self, touch):
            #Add selection on touch down 
        if super(SelectableButton, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, rv, index, is_selected):
        #Respond to the selection of items in the view. 
        self.selected = is_selected

    def on_release(self):
        
        if self.index == 0 or (self.index % 7) == 0:
            popup = EquipItemPopup(self)
            popup.open()

    def change_item(self):

        global head_id, shoulders_id, chest_id, gloves_id, weapon_id, boots_id

        if self.index == 0 or (self.index % 7) == 0:
                _SQL = "UPDATE current_items SET " + AnotherScreen.rv.selected_item_type + "_id = " + self.text + " WHERE id = 1" 
                cursor.execute(_SQL)
                conn.commit()

        head_id = getItemId('head_id')
        shoulders_id = getItemId('shoulder_id')
        chest_id = getItemId('chest_id')
        gloves_id = getItemId('gloves_id')
        weapon_id = getItemId('weapon_id')
        boots_id = getItemId('boots_id')

    def update_changes(self):
        self.change_item()

class AnotherScreen(Screen):

    class RV(BoxLayout):

        selected_item_type = ''
        selected_statistic_type = ''
        searching_text = ''
        sort_ASC_order = True

        data_items = ListProperty([])

        def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.get_data()

        def get_data(self):

            cursor.execute("SELECT * FROM head_items ORDER BY id ASC")
            rows = cursor.fetchall()
            self.selected_item_type = 'Head'
            self.selected_statistic_type = 'id'
            # create data_items
            for row in rows:
                for col in row:
                    self.data_items.append(col)

        def update_data(self, query):

            cursor.execute(query)
            rows = cursor.fetchall()

            if rows == []:

                self.data_items.clear()
                self.data_items = [0, 0, 0, 0, 0, 0, 0]

            if rows != []:

                self.data_items.clear()
            # create data_items
                for row in rows:
                    for col in row:
                        self.data_items.append(col)


    rv = RV() #obiekt RV, zeby moc updatowac tabele bazy danych


    class SortingButton(Button):

        def statistic_sort(self, selected_statistic_type):
            AnotherScreen.rv.selected_statistic_type = selected_statistic_type
            if AnotherScreen.rv.sort_ASC_order == True:
                AnotherScreen.rv.update_data("SELECT * FROM " + AnotherScreen.rv.selected_item_type + "_items" + " ORDER BY " + AnotherScreen.rv.selected_statistic_type)
                AnotherScreen.rv.sort_ASC_order = False
            else:
                AnotherScreen.rv.update_data("SELECT * FROM " + AnotherScreen.rv.selected_item_type + "_items" + " ORDER BY " + AnotherScreen.rv.selected_statistic_type + " DESC")
                AnotherScreen.rv.sort_ASC_order = True

    class SelectedSpinner(Spinner):

        def selectItems(self, selected_item_type):
            AnotherScreen.rv.selected_item_type = selected_item_type
            AnotherScreen.rv.update_data("SELECT * FROM " + AnotherScreen.rv.selected_item_type + "_items" + " ORDER BY id ASC")
    
    class SearchEquipment(TextInput):
        
        def searchItems(self, searching_text):
            
            if searching_text == '':
                AnotherScreen.rv.update_data("SELECT * FROM " + AnotherScreen.rv.selected_item_type + "_items" + " ORDER BY id ASC")
            else:
                AnotherScreen.rv.searching_text = searching_text
                AnotherScreen.rv.update_data("SELECT * FROM " + AnotherScreen.rv.selected_item_type + "_items WHERE armor_name LIKE '%" + searching_text + "%'")

    def test(self):
        return str(main_screen.parent)

class EquipmentPopup(Popup):
    statistics = Statistics()

class ScreenManagement(ScreenManager):
    pass

presentation = Builder.load_file('mainFile.kv')

class MainApp(App):

    def build(self):
        self.title = 'Database application'
        return presentation

if __name__ == '__main__':
    MainApp().run()