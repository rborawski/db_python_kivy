create database application_items_db;
use application_items_db;
create table head_items(id int NOT NULL AUTO_INCREMENT, armor_name VARCHAR(100) NOT NULL, strength int NOT NULL, dexterity int NOT NULL, vitality int NOT NULL, energy int NOT NULL, defence int NOT NULL, PRIMARY KEY(id));
INSERT INTO head_items(armor_name, strength, dexterity, vitality, energy, defence) VALUES 
						('Archon Crown', 2, 2, 4, 4, 3),
                                    ('Arming Cap', 1, 1, 1, 1, 1),
                                    ('Ascended Crown', 2, 3, 1, 4, 5),
                                    ('Basinet', 4, 4, 1, 2, 2),
                                    ('Casque', 6, 7, 2, 8, 7),
                                    ('Chain Hood', 7, 8, 5, 9, 3),
                                    ('Great Helm', 5, 5, 5, 5, 5),
                                    ('Hounskull', 3, 8, 2, 9, 5),
                                    ('Sovereign Helm', 9, 8, 7, 6, 9),
                                    ('Grojecki\'s Crown', 10, 10, 10, 10, 10);
                        
create table shoulder_items(id int NOT NULL AUTO_INCREMENT, armor_name VARCHAR(100) NOT NULL, strength int NOT NULL, dexterity int NOT NULL, vitality int NOT NULL, energy int NOT NULL, defence int NOT NULL, PRIMARY KEY(id));
INSERT INTO shoulder_items(armor_name, strength, dexterity, vitality, energy, defence) VALUES 
						('Ailettes', 2, 2, 4, 4, 3),
                                    ('Amice', 1, 1, 1, 1, 1),
                                    ('Archon Spaulders', 2, 3, 1, 4, 5),
                                    ('Balor Pauldrons', 4, 4, 1, 2, 2),
                                    ('Doom Pauldrons', 6, 7, 2, 8, 7),
                                    ('Etched Mantle', 7, 8, 5, 9, 3),
                                    ('Leather Mantle', 5, 5, 5, 5, 5),
                                    ('Shoulder Plates', 3, 8, 2, 9, 5),
                                    ('Spaulders', 9, 8, 7, 6, 9),
                                    ('Grojecki\'s Shoulders', 10, 10, 10, 10, 10);

create table chest_items(id int NOT NULL AUTO_INCREMENT, armor_name VARCHAR(100) NOT NULL, strength int NOT NULL, dexterity int NOT NULL, vitality int NOT NULL, energy int NOT NULL, defence int NOT NULL, PRIMARY KEY(id));
INSERT INTO chest_items(armor_name, strength, dexterity, vitality, energy, defence) VALUES 
						('Archon Armor', 2, 2, 4, 4, 3),
                                    ('Ascended Armor', 1, 1, 1, 1, 1),
                                    ('Astral Mail', 2, 3, 1, 4, 5),
                                    ('Battle Armor', 4, 4, 1, 2, 2),
                                    ('Doom Armor', 6, 7, 2, 8, 7),
                                    ('Hide Tunic', 7, 8, 5, 9, 3),
                                    ('Leather Doublet', 5, 5, 5, 5, 5),
                                    ('Plate Mail', 3, 8, 2, 9, 5),
                                    ('Sovereign Mail', 9, 8, 7, 6, 9),
                                    ('Grojecki\'s Chest', 10, 10, 10, 10, 10);
                        
create table gloves_items(id int NOT NULL AUTO_INCREMENT, armor_name VARCHAR(100) NOT NULL, strength int NOT NULL, dexterity int NOT NULL, vitality int NOT NULL, energy int NOT NULL, defence int NOT NULL, PRIMARY KEY(id));
INSERT INTO gloves_items(armor_name, strength, dexterity, vitality, energy, defence) VALUES 
						('Archon Gauntlets', 2, 2, 4, 4, 3),
                                    ('Ascended Gauntlets', 1, 1, 1, 1, 1),
                                    ('Balor Fists', 2, 3, 1, 4, 5),
                                    ('Battle Gauntlets', 4, 4, 1, 2, 2),
                                    ('Gauntlets', 6, 7, 2, 8, 7),
                                    ('Grips', 7, 8, 5, 9, 3),
                                    ('Leather Gloves', 5, 5, 5, 5, 5),
                                    ('Plated Gauntlets', 3, 8, 2, 9, 5),
                                    ('Vambraces', 9, 8, 7, 6, 9),
                                    ('Grojecki\'s Gloves', 10, 10, 10, 10, 10);

create table weapon_items(id int NOT NULL AUTO_INCREMENT, armor_name VARCHAR(100) NOT NULL, strength int NOT NULL, dexterity int NOT NULL, vitality int NOT NULL, energy int NOT NULL, defence int NOT NULL, PRIMARY KEY(id));
INSERT INTO weapon_items(armor_name, strength, dexterity, vitality, energy, defence) VALUES 
						('Arch Axe', 2, 2, 4, 4, 3),
                                    ('Composite Bow', 1, 1, 1, 1, 1),
                                    ('Dagger', 2, 3, 1, 4, 5),
                                    ('Crossbow', 4, 4, 1, 2, 2),
                                    ('Club', 6, 7, 2, 8, 7),
                                    ('Polearm', 7, 8, 5, 9, 3),
                                    ('Spear', 5, 5, 5, 5, 5),
                                    ('Staff', 3, 8, 2, 9, 5),
                                    ('Sword', 9, 8, 7, 6, 9),
                                    ('Grojecki\'s Glavie', 10, 10, 10, 10, 10);

create table boots_items(id int NOT NULL AUTO_INCREMENT, armor_name VARCHAR(100) NOT NULL, strength int NOT NULL, dexterity int NOT NULL, vitality int NOT NULL, energy int NOT NULL, defence int NOT NULL, PRIMARY KEY(id));
INSERT INTO boots_items(armor_name, strength, dexterity, vitality, energy, defence) VALUES 
						('Archon Greaves', 2, 2, 4, 4, 3),
                                    ('Ascended Greaves', 1, 1, 1, 1, 1),
                                    ('Balor Treads', 2, 3, 1, 4, 5),
                                    ('Battle Greaves', 4, 4, 1, 2, 2),
                                    ('Chain Boots', 6, 7, 2, 8, 7),
                                    ('Doom Treads', 7, 8, 5, 9, 3),
                                    ('Heavy Boots', 5, 5, 5, 5, 5),
                                    ('Heavy Sabatons', 3, 8, 2, 9, 5),
                                    ('Silk Shoes', 9, 8, 7, 6, 9),
                                    ('Grojecki\'s Boots', 10, 10, 10, 10, 10);

create table current_items(id INT NOT NULL AUTO_INCREMENT, head_id INT, shoulder_id INT, chest_id INT, gloves_id INT, weapon_id INT, boots_id INT, 
						PRIMARY KEY(id), 
						FOREIGN KEY(head_id) REFERENCES head_items(id),
                                    FOREIGN KEY(shoulder_id) REFERENCES shoulder_items(id),
                                    FOREIGN KEY(chest_id) REFERENCES chest_items(id),
                                    FOREIGN KEY(gloves_id) REFERENCES gloves_items(id),
                                    FOREIGN KEY(weapon_id) REFERENCES weapon_items(id),
                                    FOREIGN KEY(boots_id) REFERENCES boots_items(id)
                                    );